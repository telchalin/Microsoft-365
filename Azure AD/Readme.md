## Azure AD scripts

<p><b>Install-BaselineCAPolicies.ps1</b> -This script will import my recommended baseline policies for Conditional Access
<p><b>Enable-MfaForLicensedUsers.ps1</b> - Use this to enable MFA for all licensed users (works with all subscriptions) (Don't USE)
<p><b>Disable-MfaForLicensedUsers.ps1</b> - Use this to switch from per-user MFA to Conditional Access
<p><b>Enable-SensitivityLabelsForGroups.ps1</b> - Use this to enable Sensitivity labels for Microsoft 365 Groups and SharePoint Sites
<p><b>Limit-GroupsCreation.ps1</b> - Use this to limit the ability to create Microsoft 365 Groups to a specific security group
<p><b>Set-GroupExpirationPolicy.ps1</b> - Use this to set group expiration policy
